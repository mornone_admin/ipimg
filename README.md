# 根据ip自动生成图片

#### 介绍
根据访客ip自动生成一张包含多种信息的图片，包含：地理位置、当前日期、ip地址、终端操作系统、浏览器版本。


#### 使用方法
可以直接上传根目录访问，也可是二级目录

#### 调用示例

`<img src="http://demo.hezi.moqingwu.com/ipimg/" height="150">`

可自行修改高度

#### 效果图

![输入图片说明](https://images.gitee.com/uploads/images/2018/1224/220558_77154f96_2236313.png "20180526175459.png")


#### 演示地址

http://demo.hezi.moqingwu.com/ipimg/


#### 更多源码

http://hezi.moqingwu.com/